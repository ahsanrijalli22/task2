class Car {
  constructor(make, speed) {

      this.make = make;
      this.speed = speed;

  }
  accelerate() {

      this.speed += 5;
      console.log(`${this.make} is going at ${this.speed} km/h`);
  }
  brake() {

      this.speed -= 5;
      console.log(`${this.make} is going at ${this.speed} km/h`);
  }
}



let bmw = new Car("bmw", 125);
let mercedes = new Car("mercedes", 100);

bmw.accelerate();
mercedes.accelerate();
bmw.brake();
mercedes.brake();

/**
* Output: bmw is going 130
* Output: mercedes is going 105
* Output: bmw is going 125
* Output: mercedes is going 100
*/